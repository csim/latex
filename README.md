# LaTeX

Exemplo de como fazer um project LaTeX usando a integração CI/CD do Gitlab.

O arquivo ```.gitlab-ci.yml``` controla o CI/CD, basta alterá-lo e trocar ```mwe.tex``` pelo arquivo do seu projeto e depois ```mwe.pdf``` pelo arquivo PDF gerado
